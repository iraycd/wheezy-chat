from socketio import socketio_manage
from wheezy.http.response import HTTPResponse
from wheezy.http.response import not_found
from wheezy.web.handlers import BaseHandler

from chat import ChatNamespace
from models import ChatRoom
from models import get_object
from models import get_or_create
from models import session


class RoomsHandler(BaseHandler):

    def get(self):
        rooms = session.query(ChatRoom).all()
        return self.render_response('rooms.html',
                                    rooms=rooms)


class RoomHandler(BaseHandler):

    def get(self):
        room = get_object(ChatRoom, slug=self.route_args['slug'])
        if not room:
            return not_found()
        return self.render_response('room.html',
                                    room=room)


class CreateHandler(BaseHandler):

    def post(self):
        name = self.request.form.get("name")[0]
        if name:
            room, created = get_or_create(ChatRoom, name=name)
            return self.redirect_for('room', slug=str(room.slug))
        return self.redirect_for('rooms')


def socketio(request):
    socketio_manage(request.environ, {'/chat': ChatNamespace}, request)
    return HTTPResponse()
