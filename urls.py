""" ``urls`` module.
"""

from wheezy.routing import url
from wheezy.http.response import not_found
from wheezy.web.handlers import file_handler

from views import CreateHandler
from views import RoomHandler
from views import RoomsHandler
from views import socketio


all_urls = [
    url('', RoomsHandler, name='rooms'),
    url('create', CreateHandler, name='create'),
    url('socket.io/{path:any}', socketio, name='socketio'),
    url('static/{path:any}',
        file_handler(root='static/'),
        name='static'),
    url('favicon.ico', lambda r: not_found()),
    url('{slug}', RoomHandler, name='room')
]
