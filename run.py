from gevent import monkey
from socketio.server import SocketIOServer

from app import main

monkey.patch_all()

PORT = 8000

if __name__ == '__main__':
    SocketIOServer(('', PORT), main, resource="socket.io").serve_forever()
