import re

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker


engine = create_engine('sqlite:////tmp/chat.db')
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


# regiom: models
#
class ChatRoom(Base):

    __tablename__ = 'chatrooms'
    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    slug = Column(String(50))
    users = relationship('ChatUser', backref='chatroom', lazy='dynamic')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return "/" + self.slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        session.add(self)
        session.commit()


class ChatUser(Base):

    __tablename__ = 'chatusers'
    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    session = Column(String(20), nullable=False)
    chatroom_id = Column(Integer, ForeignKey('chatrooms.id'))

    def __unicode__(self):
        return self.name


# region: utils

def slugify(value):
    value = unicode(re.sub('[^\w\s-]', '', value).strip().lower())
    return re.sub('[-\s]+', '-', value)


def get_object(klass, **query):
    instance = session.query(klass).filter_by(**query).first()
    if not instance:
        return False
    return instance


def get_or_create(klass, **kwargs):
    o = get_object(klass, **kwargs), False
    if o[0]:
        return o
    instance = klass(**kwargs)
    instance.save()
    return instance, True


def init_db():
    Base.metadata.create_all(engine)
