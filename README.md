#Wheezy Chat


Wheezy implementation of chat from [gevent-socketio](https://github.com/abourget/gevent-socketio) examples.

Forked from [flask_chat](https://github.com/abourget/gevent-socketio/tree/master/examples/flask_chat)

To Run this example, before you start you need to navigate to this folder and run:

`python init_db.py`

Then to run use:

`python run.py`

Open your browser to:

`localhost:8000`


And everything should be working now.  If you want to reset just stop the server, delete the file: `/tmp/chat.db`, and re-run `init_db.py`



